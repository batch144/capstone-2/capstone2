
const Order = require("./../models/Order");
const User = require("./../models/User");
const Product = require("./../models/Product");

const bcrypt = require("bcrypt");
const auth = require("./../auth");

// ADMIN - get all orders
module.exports.getAllOrders = ()=>{

	return Order.find().then((result, err) => {

		if(err) {
			return false
		} else {
			return result
		}
	})
}

// Checkout
module.exports.checkout = (data) => {

	const {isAdmin, userId, productId} = data

	return Order.findOne({userId: userId}).then((result, error) => {
		// console.log(result)

		if (result == null && isAdmin == false){

			return Product.findById(productId).then((res, err) => {
				// console.log(res)

				let quantity = 1;
				let subTotal = res.price * quantity;
				let name = res.name;
				let price = res.price;

				if (err) {
					return err
				} else {

					let newOrder = new Order({
						userId: userId,
						orderedItems: [{name, price, quantity, subTotal}],
						totalPrice: res.price
					})

					return newOrder.save().then( (res, err) => {

						if(err){
							return false
						} else {
							return res
						}
					})
				}
			})


		} else if(result && isAdmin == false){

			return Product.findById(productId).then((res, err) => {
				// console.log(result)
				// console.log(res)

				let quantity = 1;
				let subTotal = res.price * quantity;
				let name = res.name;
				let price = res.price;

				const existingProductIndex = result.orderedItems.findIndex(p => p.name == res.name);
				const exsitingProduct = result.orderedItems[existingProductIndex];

				if (existingProductIndex >= 0) { // exist in orderedItems already
	         
	            	exsitingProduct.quantity += 1;
	            	exsitingProduct.subTotal = res.price * exsitingProduct.quantity;
	        	
	        	} else { //not exist
	            	
	            	result.orderedItems.push({name, price, quantity, subTotal});
	        	}

	        	result.totalPrice += res.price;

	        	return result.save().then( (res, err) => {
					if(err){
						return false
					} else {
						return res
					}
				})

			})

		} else {

			return false
		}

	})
}

//Get user order
module.exports.getCart = (data) => {
	const {id} = data

	let userOrder = {
		userId: id
	}

	return Order.findOne(userOrder).then((result, error) => {
		if (result) {
			return result
		} else {
			return error
		}
	})
}


//ADMIN- delete order
module.exports.deleteOrder = (id) => {

	return Order.findByIdAndDelete(id).then((result, error) => {

		if(result == null){
			return `Order not exist`
		} else {
			if(result){
				return "Order deleted"
			} else {
			return error
			}
		}
	})
}

//Decrease Quantity
module.exports.decQty = (data) => {

	const {userId, productId} = data

	return Order.findOne({userId: userId}).then((result, error) => {

		return Product.findById(productId).then((res, err) => {
			console.log(res)

			let quantity = 1;
			let subTotal = res.price * quantity;
			let price = res.price;

			const existingProductIndex = result.orderedItems.findIndex(p => p.name == res.name);
			const exsitingProduct = result.orderedItems[existingProductIndex];

			if (existingProductIndex >= 0) { // exist in orderedItems already
		         
	        	if(exsitingProduct.quantity > 1){
	        		exsitingProduct.quantity -= 1;
	           		exsitingProduct.subTotal = res.price * exsitingProduct.quantity;
	        	}
		        	
	      	} else { //not exist
		            	
		     	return error
		   	}

		    result.totalPrice += res.price;

		    return result.save().then( (res, err) => {
				if(err){
					return false
				} else {
					return res
				}
			})

		})
	})
}

//Increase Quantity
module.exports.incQty = (data) => {

	const {userId, productId} = data

	return Order.findOne({userId: userId}).then((result, error) => {

		return Product.findById(productId).then((res, err) => {
			// console.log(res)

			let quantity = 1;
			let subTotal = res.price * quantity;
			let price = res.price;

			const existingProductIndex = result.orderedItems.findIndex(p => p.name == res.name);
			const exsitingProduct = result.orderedItems[existingProductIndex];

			if (existingProductIndex >= 0) { // exist in orderedItems already
		         
	        	exsitingProduct.quantity += 1;
	           	exsitingProduct.subTotal = res.price * exsitingProduct.quantity;
	        	
	      	} else { //not exist
		            	
		     	return error
		   	}

		    result.totalPrice += res.price;

		    return result.save().then( (res, err) => {
				if(err){
					return false
				} else {
					return res
				}
			})

		})
	})
}
