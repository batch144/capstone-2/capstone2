
const auth = require("./../auth")
const User = require("./../models/User");
const Product = require("./../models/Product");
const Order = require("./../models/Order");


//create products
module.exports.createProduct = (reqBody) => {

			let newProduct = new Product({
				name: reqBody.name,
				description: reqBody.description,
				price: reqBody.price
			});

			return newProduct.save().then((result, err) => {
				if(err) {
					return false
				} else {
					return true
				}
			})
}

//get all products
module.exports.getAllproducts = () => {

	return Product.find().then((result, error) => {
		if(error){
			return false
		} else {
			return result
		}
	})
}

//get specific product
module.exports.getProductById = (params) => {

	return Product.findById(params).then((result, error) => {
		if(result === null){
			return `Product not exist`
		} else {
			if(result){
				return result
			} else {
			return error
			}
		}
	})
}

//ADMIN - Update Product info
module.exports.updateInfo = (params, reqBody) => {
	
	let {name, description, price} = reqBody
		const updatedProduct = {
			name: name,
			description: description,
			price: price
		}
			
	return Product.findByIdAndUpdate(params, updatedProduct, {new: true}).then((result, error) => {
		if(error) {
			return error
		} else {
			return true
		}
    })
}

//ADMIN - archive product
module.exports.archiveProduct = (params) => {
	
	return Product.findByIdAndUpdate(params, {isActive: false}).then((result, err) => {
		if(result == null){
			return `Product does not exist`
		} else {
			if(result){
				return true
			} else {
				return false
			}
		}
	})
}

//ADMIN - unarchive product
module.exports.unarchiveProduct = (params) => {
	
	return Product.findByIdAndUpdate(params, {isActive: true}).then((result, err) => {
		if(result == null){
			return `Product does not exist`
		} else {
			if(result){
				return true
			} else {
				return false
			}
		}
	})
}

//ADMIN - delete product
module.exports.deleteProduct = (params) => {

	return Product.findByIdAndDelete(params).then((result, err) => {
		if(result == null){
			return `Product does not exist`
		} else {
			if(result){
				return true
			} else {
				return false
			}
		}
	})
}