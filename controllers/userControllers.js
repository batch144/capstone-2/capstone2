
const User = require("./../models/User");
const bcrypt = require("bcrypt");
const auth = require("./../auth");
const Product = require("./../models/Product");
const Order = require("./../models/Order");


//Check email exist
module.exports.checkEmail = (reqBody) => {
	
	const {email} = reqBody

	return User.findOne({email: email}).then((result, err) => {
		if(result != null) {
			return true;
		} else {
			if(err) {
				return err;
			} else {
				return false;
			}
		}
	})
}

//retrieve all users
module.exports.getAllUsers = (data) => {
	
	return User.findById(data).then((result, error) => {
		if(result.isAdmin == false) {
			return "You must be an admin"
		} else {
			return User.find().then((result, err) => {
				if(err) {
					return false
				} else {
					result.password = "";
					return result;
				}
			})
		}
	})
}

//register
module.exports.register = (reqBody) => {

	let newUser = new User({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 5)
	})
	//save()
	return User.findOne({email: reqBody.email}).then((result, err) => {
		if(result !== null) {
			return `Email already existing!`
		} else {
			return newUser.save().then((result, err) => {
				if(err) {
					return false;
				} else {
					return true;
				}
			});
		}
	})
}

//login
module.exports.login = (reqBody) => {
	const {email, password} = reqBody;
	return User.findOne({email: email}).then( (result, error) => {

		if(result == null){
			return false
		} else {

			let isPasswordCorrect = bcrypt.compareSync(password, result.password)

			if(isPasswordCorrect == true){
				return {access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
}

//Get User Profile
module.exports.getProfile = (reqBody) => {
	
	return User.findById(reqBody).then((result, err) => {
		if(result == null){
			return false;
		} else {
			result.password = "";
			return result
		}
	})
}

//ADMIN - set user as admin
module.exports.setAsAdmin = (params) => {

    return User.findByIdAndUpdate(params, {isAdmin: true}).then( (result, error) => {

        if(result == null){
            return `User does not exist`
        } else {
            if(result){
                return result
            } else {
                return false
            }
        }
    })
}

//ADMIN - Unset user as admin
module.exports.unSetAsAdmin = (params) => {
	
	return User.findByIdAndUpdate(params, {isAdmin: false}).then( (result, error) => {

        if(result == null){
            return `User does not exist`
        } else {
            if(result){
                return result
            } else {
                return false
            }
        }
    })
}

