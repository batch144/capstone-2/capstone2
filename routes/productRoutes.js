
const express = require("express");
const router = express.Router();
const productControllers = require("./../controllers/productControllers");
const auth = require("./../auth");


//create a product
router.post("/createProduct", auth.verify, (req, res) => {
	
	let verifiedUser = auth.decode(req.headers.authorization)
    if(verifiedUser.isAdmin){
        productControllers.createProduct(req.body).then(result => res.send(result))
    } else {
        res.send("You must be an admin")
    }
})

//get all products
router.get("/", (req, res) => {
	
	productControllers.getAllproducts().then(result => res.send(result))
})

//get specific product
router.get("/:productId", (req, res) => {

	productControllers.getProductById(req.params.productId).then( result => res.send(result))
})

//ADMIN - Update Product info
router.put("/:productId/editProduct", auth.verify, (req, res) => {

	let verifiedUser = auth.decode(req.headers.authorization)
	if(verifiedUser.isAdmin){
        productControllers.updateInfo(req.params.productId, req.body).then(result => res.send(result))
    } else {
        res.send("You must be an admin")
    }
})

//ADMIN - archive product
router.put("/:productId/archive", auth.verify, (req, res) => {

	let verifiedUser = auth.decode(req.headers.authorization)
	if(verifiedUser.isAdmin){
        productControllers.archiveProduct(req.params.productId).then( result => res.send(result))
    } else {
        res.send("You must be an admin")
    }
})

//ADMIN - unarchive product
router.put("/:productId/unarchive", auth.verify, (req, res) => {

	let verifiedUser = auth.decode(req.headers.authorization)
	if(verifiedUser.isAdmin){
        productControllers.unarchiveProduct(req.params.productId).then( result => res.send(result))
    } else {
        res.send("You must be an admin")
    }
})

//ADMIN - delete product
router.delete("/:productId/deleteProduct", auth.verify, (req, res) => {
	
	let verifiedUser = auth.decode(req.headers.authorization)
	if(verifiedUser.isAdmin){
        productControllers.deleteProduct(req.params.productId).then(result => res.send(result))
    } else {
        res.send("You must be an admin")
    }
})


module.exports = router;