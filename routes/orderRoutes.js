
const express = require("express");
const router = express.Router();

const orderControllers = require("./../controllers/orderControllers");
const auth = require("./../auth")


// ADMIN - get all orders
router.get("/", auth.verify, (req, res) => {
	
	let verifiedUser = auth.decode(req.headers.authorization)
    if(verifiedUser.isAdmin){
        orderControllers.getAllOrders().then(result => res.send(result))
    } else {
        res.send("You must be an admin")
    }
})

// Create order
router.post("/checkout/:productId", auth.verify, (req, res) => {
	
	const data = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin,
		userId : auth.decode(req.headers.authorization).id,
		productId: req.params.productId
	}
	orderControllers.checkout(data).then(result => res.send(result))
})

//Get user order
router.get("/getCart", auth.verify, (request, response) => {
	let data = auth.decode(request.headers.authorization)
	orderControllers.getCart(data).then(result => response.send(result));
})

//ADMIN- delete order
router.delete("/:orderId/deleteOrder", auth.verify, (req, res) => {

	let verifiedUser = auth.decode(req.headers.authorization)
    if(verifiedUser.isAdmin){
        orderControllers.deleteOrder(req.params.orderId).then( result => res.send(result))
    } else {
        res.send("You must be an admin")
    } 
})

//Decrease Quantity
router.post("/decQty/:productId", auth.verify, (req, res) => {
	
	const data = {
		userId : auth.decode(req.headers.authorization).id,
		productId: req.params.productId
	}
	orderControllers.decQty(data).then(result => res.send(result))
})

//Increase Quantity
router.post("/incQty/:productId", auth.verify, (req, res) => {
	
	const data = {
		userId : auth.decode(req.headers.authorization).id,
		productId: req.params.productId
	}
	orderControllers.incQty(data).then(result => res.send(result))
})


module.exports = router;