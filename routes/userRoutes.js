
const express = require("express");
const router = express.Router();

const userControllers = require("./../controllers/userControllers");
const auth = require("./../auth")

//Check email exist
router.post("/emailExists", (req, res) => {
	
	userControllers.checkEmail(req.body).then(result => res.send(result))
})

//retrieve all users
router.get("/", auth.verify, (req, res) => {
	
	let userData = auth.decode(req.headers.authorization).id
	userControllers.getAllUsers(userData).then(result => res.send(result));
});

//register a user
router.post("/register", (req, res) => {

	userControllers.register(req.body).then(result => res.send(result))
})

//login
router.post("/login", (req, res) => {

	userControllers.login(req.body).then(result => res.send(result))
})

//Get User Profile
router.get("/userProfile", auth.verify, (req, res) => {
	let userData = auth.decode(req.headers.authorization).id
	userControllers.getProfile(userData).then(result => res.send(result));
});

//ADMIN - set user as admin
router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {
    
    let userData = auth.decode(req.headers.authorization)
    if(userData.isAdmin){
        userControllers.setAsAdmin(req.params.userId).then(result => res.send(result))
    } else {
        res.send("You must be an admin")
    }
})

//ADMIN - unset user as admin
router.put("/:userId/unSetAsAdmin", auth.verify, (req, res) => {

	let userData = auth.decode(req.headers.authorization)
    if(userData.isAdmin){
        userControllers.unSetAsAdmin(req.params.userId).then(result => res.send(result))
    } else {
        res.send("You must be an admin")
    }
})

module.exports = router;