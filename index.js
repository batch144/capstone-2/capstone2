
//import express module
const express = require("express");
const mongoose = require("mongoose");

// express server
const app = express();
//local port and available port
const PORT = process.env.PORT || 4000;
const cors = require("cors");

//import routes module
const productRoutes = require("./routes/productRoutes");
const userRoutes = require("./routes/userRoutes");
const orderRoutes = require("./routes/orderRoutes");


//mongodb connection
mongoose.connect('mongodb+srv://admin:admin@batch139.idzre.mongodb.net/online-ordering-system?retryWrites=true&w=majority',
	{useNewUrlParser: true, useUnifiedTopology: true});

//mongodb notification
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log(`Connected to database`));


//middleware
app.use(express.json())
app.use(express.urlencoded({extended: true}))
app.use(cors({
	origin: ["http://localhost:3000"],
	methods: ["GET", "POST", "DELETE", "PUT"],
	allowedHeaders: ["Content-Type", "Authorization", "x-access-token"],
	credentials: true,
	exposedHeaders: ["set-cookie"],
	})
);

//routes
app.use("/api/products", productRoutes);
app.use("/api/users", userRoutes);
app.use("/api/orders", orderRoutes);



app.listen(PORT, () => console.log(`Server running at port ${PORT}`));