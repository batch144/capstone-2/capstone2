
const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema(
	{
		userId: {
			type: String,
			required: [true, "User ID is required"]
		},

		totalPrice: {
			type: Number
		},

		purchasedOn: {
			type: Date,
			default: new Date()
		},
		
		orderedItems: [
			{
				name: {
					type: String,
					required: [true, "Product Name is required"]
				},
				price: {
					type: Number,
					required: [true, "Price is required"]
				},
				quantity: {
					type: Number,
					default: 1
				},
				subTotal: {
					type: Number
				}
			}
		]
	}

)

module.exports = mongoose.model("Order", orderSchema);
